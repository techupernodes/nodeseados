const express = require('express');
const serveStatic = require("serve-static");
const path = require('path');
const app = express();
app.use(serveStatic(path.join(__dirname, 'build/default')));

const port = process.argv[2];
app.listen(port, function(){
    console.log('Se escucha en el puerto ' + port);
});